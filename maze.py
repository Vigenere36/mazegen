import random, sys

class Maze:
	def __init__(self, dimX, dimY):
		self.dimX = dimX
		self.dimY = dimY
		self.start = (0, 0)
		self.end = (dimX - 1, dimY - 1)
		self.path = [[]] * dimX
		self.visited = [[]] * dimX
		for i in range(dimX):
			self.path[i] = [False] * dimY
			self.visited[i] = [False] * dimY

	def neighbors(self, x, y):
		count = 0
		if x + 1 < self.dimX and self.path[x + 1][y]:
			count += 1
		if x - 1 >= 0 and self.path[x - 1][y]:
			count += 1
		if y + 1 < self.dimY and self.path[x][y + 1]:
			count += 1
		if y - 1 >= 0 and self.path[x][y - 1]:
			count += 1
		return count

	def validPath(self, x, y):
		if x == self.dimX - 1 and y == self.dimY - 1: return True #end
		if x < 0 or x >= self.dimX: return False
		if y < 0 or y >= self.dimY: return False
		if self.visited[x][y]: return False
		return True

	def randDirec(self, x, y):
		if x + 1 == self.dimX - 1 and y == self.dimY - 1: return (self.dimX - 1, self.dimY - 1)
		if x - 1 == self.dimX - 1 and y == self.dimY - 1: return (self.dimX - 1, self.dimY - 1)
		if x == self.dimX - 1 and y + 1 == self.dimY - 1: return (self.dimX - 1, self.dimY - 1)
		if x == self.dimX - 1 and y - 1 == self.dimY - 1: return (self.dimX - 1, self.dimY - 1)

		roll = []
		if self.validPath(x - 1, y) and self.neighbors(x - 1, y) < 2:
			roll.append((x - 1, y))
			roll.append((x - 1, y))
			roll.append((x - 1, y)) #biasing away from exit
		if self.validPath(x, y - 1) and self.neighbors(x, y - 1) < 2:
			roll.append((x, y - 1)) 
			roll.append((x, y - 1)) 
			roll.append((x, y - 1)) #biasing away from exit
		if self.validPath(x + 1, y) and self.neighbors(x + 1, y) < 2:
			roll.append((x + 1, y))
		if self.validPath(x, y + 1) and self.neighbors(x, y + 1) < 2:
			roll.append((x, y + 1))

		if not roll: return -1
		return roll[random.randint(0, len(roll) - 1)]

	def createPath(self):
		stack = []
		curr = self.start
		while not self.path[self.end[0]][self.end[1]]:
			x, y = curr[0], curr[1]
			self.visited[x][y] = True
			self.path[x][y] = True
			if curr == self.end: break
			roll = self.randDirec(x, y)
			if roll == -1:
				if random.randint(0, 5) < 5: self.path[x][y] = False #biasing connections. < n -- larger number = less path intersections
				curr = stack.pop()
			else:
				stack.append(curr)
				curr = roll

	def printMaze(self):
		for y in range(self.dimY):
			if y == 0:
				print '_' * (self.dimX + 2)
			row = '|'
			for x in range(self.dimX):
				if x == 0 and y == 0: row += 'S'
				elif x == self.dimX - 1 and y == self.dimY - 1: row += 'E'
				elif self.path[x][y]: row += ' '
				else: row += '#'
			row += '|'
			print row
		print '-' * (self.dimX + 2)

def createMaze(dimX, dimY):
	maze = Maze(dimX, dimY)
	maze.createPath()
	maze.printMaze()

def main(args):
	if len(args) != 2:
		print 'Usage: maze <x dimension> <y dimension>'
		return
	createMaze(int(args[0]), int(args[1]))

if __name__ == "__main__":
	main(sys.argv[1:])